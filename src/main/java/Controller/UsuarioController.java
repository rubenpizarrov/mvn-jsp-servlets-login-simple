package Controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAL.UsuarioColeccion;
import Model.Usuario;

/**
 * Servlet implementation class UsuarioController
 */
public class UsuarioController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	UsuarioColeccion usuarios = new UsuarioColeccion();   
    
	
	/**
     * @see HttpServlet#HttpServlet()
     */
    public UsuarioController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String opcionBtn = request.getParameter("btnRegistrar");
        String opcionBtnLogin = request.getParameter("btnLogin");

        if (opcionBtn != null && opcionBtn.equals("Registrar")) {
            String nombre = request.getParameter("txtNombre").trim();
            String apellido = request.getParameter("txtApellido").trim();
            String telefono = request.getParameter("txtTelefono").trim();
            String correo = request.getParameter("txtCorreo").trim();
            String password1 = request.getParameter("txtPassword1").trim();
            String password2 = request.getParameter("txtPassword2").trim();

            Usuario u = new Usuario(nombre, apellido, telefono, correo, password1);

            usuarios.agregarUsuario(u);

            if (password1.equals(password2)) {
                request.getSession().setAttribute("msgRegistro", "Usuario registrado exitósamente");
                request.getSession().setAttribute("lista", usuarios.obtenerListaUsuarios());
                response.sendRedirect("registro.jsp");
            } else {
                request.getSession().setAttribute("msg", "Ha habido un problema Huston");
                response.sendRedirect("registro.jsp");
            }
        }

        ///Login
        if (opcionBtnLogin != null && opcionBtnLogin.equals("btnLogin")) {
            String user = request.getParameter("txtUsuario");
            String pass = request.getParameter("txtPassword");
            Usuario usu = null;
            ArrayList<Usuario> listaUsuarios = usuarios.obtenerListaUsuarios();

            for (Usuario usuario : listaUsuarios) {
                if (usuario.getCorreo().equals(user) && usuario.getPassword().equals(pass)) {
                    usu = usuario;
                }
            }

            if (usu != null) {
                request.getSession().setAttribute("usuario", usu);
                request.getSession().setAttribute("msg", "");
                response.sendRedirect("ventas.jsp");
            } else {
                request.getSession().setAttribute("msgLogin", "El usuario es erroneo o no está registrado");
                response.sendRedirect("login.jsp");
            }

        }
		
	}

}
