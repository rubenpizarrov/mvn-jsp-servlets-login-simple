package DAL;

import java.util.ArrayList;

import Model.Usuario;

public class UsuarioColeccion {
	ArrayList<Usuario> listaUsuarios = new ArrayList<>();

    public ArrayList<Usuario> obtenerListaUsuarios() {
        return listaUsuarios;
    }
    
    public void agregarUsuario(Usuario usu){
        listaUsuarios.add(usu);
    }
}
