<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <jsp:include page="header.jsp"></jsp:include>
    <section id="blanca">
        <div class="container-fluid">
            <div class="row justify-content-center section-title mb-5">
                <h2 class="section-title-heading">Debe Ingresar con su Usuario</h2>
            </div>
            <div class="row">
                <div class="col"></div>
                <div class="col-6" id="col-formulario">
                    <form action="UsuarioController" method="post" id="form-validate" class="needs-validation" novalidate>
                        <h2>Login Cliente</h2>
                        <div class="alert alert-info" role="alert">
                                ${msg}
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtUsuario">Correo</label>
                                    <input type="text" class="form-control" id="txtNombre" name="txtUsuario" placeholder="correo@corre.cl" required>
                                    <div class="invalid-feedback">Solo se permiten letras en este campo</div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtPassword">Password</label>
                                    <input type="text" class="form-control" id="txtApellido" name="txtPassword" required>
                                    <div class="invalid-feedback">Solo se permiten letras en este campo</div>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <div class="d-flex justify-content-end">
                                <button type="reset" class="btn btn-secondary" name="btnLimpiar">Limpiar</button>
                                <button type="submit" class="btn btn-primary"  name="btnLogin" value="btnLogin">Ingresar</button>
                        </div>
                       
                    </form>
                </div>
                <div class="col"></div>
            </div>
        </div>

    </section>
    
    
    
    <jsp:include page="footer.jsp"></jsp:include>
    
    </body>
</html>