<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <jsp:include page="header.jsp"></jsp:include>
    
    
    <section id="blanca">
        <div class="container-fluid">
            <div class="row justify-content-center section-title mb-5">
                <h2 class="section-title-heading">Registro CLientes</h2>
            </div>
            <div class="row">
                <div class="col"></div>
                <div class="col-6" id="col-formulario">
                    <form action="UsuarioController" method="post" id="form-validate" class="needs-validation" novalidate>
                        <h2>Registro de Cliente</h2>
                        <div class="alert alert-info" role="alert">
                        ${msgRegistro}
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtNombre">Nombre</label>
                                    <input type="text" class="form-control" id="txtNombre" name="txtNombre" required>
                                    <div class="invalid-feedback">Solo se permiten letras en este campo</div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtApellido">Apellido</label>
                                    <input type="text" class="form-control" id="txtApellido" name="txtApellido" required>
                                    <div class="invalid-feedback">Solo se permiten letras en este campo</div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtTelefono">Teléfono</label>
                                    <input type="text" class="form-control" id="txtTelefono" name="txtTelefono" required>
                                    <div class="invalid-feedback">Solo se permiten letras en este campo</div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtCorreo">Correo</label>
                                    <input type="text" class="form-control" id="txtCorreo" name="txtCorreo" required>
                                    <div class="invalid-feedback">Solo se permiten letras en este campo</div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtPassword1">Password</label>
                                    <input type="password" class="form-control" id="txtPassword1" name="txtPassword1" required>
                                    <div class="invalid-feedback">Debe ser mato a 8 caracteres</div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <label for="txtPassword2">Repita el Password</label>
                                    <input type="password" class="form-control" id="txtPassword2" name="txtPassword2" required>
                                    <div class="invalid-feedback">Solo se permiten letras en este campo</div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="d-flex justify-content-end">
                                <button type="reset" class="btn btn-secondary" name="btnLimpiar">Limpiar</button>
                                <button type="submit" class="btn btn-primary"  id="btnRegistrar" name="btnRegistrar" value="Registrar">Registrar</button>
                        </div>
                       
                    </form>
                </div>
                <div class="col"></div>
            </div>
        </div>

    </section>
    
    <jsp:include page="footer.jsp"></jsp:include>
    <script src="js/validations.js"></script>
    </body>
</html>
